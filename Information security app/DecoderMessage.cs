﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Information_security_app
{
    class DecoderMessage
    {
        private static int _number_d;
        private static int _module_n;

        private List<int> decoderMessage = new List<int>();
        private const string alphabet = " абвгдеёжзийклмнопрстуфхцчшщьыъэюя0123456789.,:!?";

        public static void GetPrivateKey(int d, int n)
        {
            _number_d = d;
            _module_n = n;
        }

        public void StartDecoder(string message)
        {
            decoderMessage.AddRange(message.Split(' ').Select(int.Parse));
            Decoder();
        }

        private void Decoder()
        {
            for (int i = 0; i < decoderMessage.Count; i++)
            {
                int symbol = decoderMessage[i];
                BigInteger degree = BigInteger.Pow(symbol, _number_d);
                BigInteger decoder = degree % _module_n;
                char s = alphabet[(int)decoder - 1];

                Window.window.DataOutput(Convert.ToString(s), Window.OutputMethod.newMessage);
            }
        }
    }
}
