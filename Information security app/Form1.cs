﻿using System;
using System.Windows.Forms;

/// <summary>
/// http://www.michurin.net/computer-science/rsa.html
/// </summary>
namespace Information_security_app
{
    public partial class Window : Form
    {
        //PrimeNumber 
        private int _primeNumber_P;
        private int _primeNumber_Q;

        private int _theNumberOfDivisionsWithoutRemainder;//кол-во делений без остатка 
        private bool _isPrimeNumber;

        private KeyGenerator keyGenerator = new KeyGenerator();
        public static Window window = null;

        public Window()
        {
            InitializeComponent();
            window = this;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            {
                ToolTip _infoPrimeNumber = new ToolTip();
                _infoPrimeNumber.SetToolTip(label1, "Просто́е число́ — натуральное (целое положительное) число, " +
                    "\nимеющее ровно два различных натуральных делителя — единицу и самого себя\n" +
                    "2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, \n" +
                    "79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, \n" +
                    "167, 173, 179, 181, 191, 193, 197, 199");
            }
        }

        public void Generate_Click(object sender, EventArgs e)
        {
            this.Cursor = System.Windows.Forms.Cursors.AppStarting;

            try
            {
                _primeNumber_P = Convert.ToInt32(textBoxNumberP.Text);
                _primeNumber_Q = Convert.ToInt32(textBoxNumberQ.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Введены не верные данные", "Error");
                return;
            }
            CheckTheNumber(_primeNumber_P);
            if (_isPrimeNumber)
            {
                CheckTheNumber(_primeNumber_Q);
                if (_isPrimeNumber)
                {
                    infoLabel.Text = "Числа прошли проверку \nи являются простыми";
                    keyGenerator.CreatingAPublicKey(_primeNumber_P, _primeNumber_Q, 1);
                    textBoxMessage.ReadOnly = false;
                }
            }

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        private bool CheckTheNumber(int numberBeingChecked)
        {
            if (numberBeingChecked > 1)
            {
                _theNumberOfDivisionsWithoutRemainder = 0;

                for (int i = 1; i <= numberBeingChecked; i++)
                {
                    if (numberBeingChecked % i == 0)
                    {
                        _theNumberOfDivisionsWithoutRemainder++;
                    }
                }

                if (_theNumberOfDivisionsWithoutRemainder > 2)
                {
                    MessageBox.Show("Число " + numberBeingChecked + " не является простым", "Error");
                    return _isPrimeNumber = false;
                }

                else
                {
                    return _isPrimeNumber = true;
                }
            }

            else
            {
                infoLabel.Text += "error";
                return _isPrimeNumber = false;
            }
        }

        private void EncryptionButton_Click(object sender, EventArgs e)
        {
            encryptedDataText.Text = String.Empty;
            MessageEncryption messageEncryption = new MessageEncryption();
            messageEncryption.PrepareMessage(textBoxMessage.Text);
        }

        private void DecoderButton_Click(object sender, EventArgs e)
        {
            /*
             * Так как последний элемент переданной строки пробел, 
             * его нужно удалить иначе в классе где производится
             * расшифровка полетят ошибки
             */
            string str = encryptedDataText.Text;
            str = str.Remove(str.Length - 1);
            newMessage.Text = String.Empty;
            DecoderMessage decoder = new DecoderMessage();
            decoder.StartDecoder(str);
        }

        public void DataOutput(string dataText,  OutputMethod outputMethod)
        {
            if (outputMethod == OutputMethod.info) infoLabel.Text += dataText;
            if (outputMethod == OutputMethod.encrypted) encryptedDataText.Text += dataText;
            if (outputMethod == OutputMethod.newMessage) newMessage.Text += dataText;
            if (outputMethod == OutputMethod.exception) MessageBox.Show(dataText, "Error!");
        }

        public enum OutputMethod
        {
            info,
            encrypted,
            newMessage,
            exception
        }
    }
}
