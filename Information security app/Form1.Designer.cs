﻿
namespace Information_security_app
{
    partial class Window
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Window));
            this.textBoxNumberP = new System.Windows.Forms.TextBox();
            this.textBoxNumberQ = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Generate = new System.Windows.Forms.Button();
            this.infoLabel = new System.Windows.Forms.Label();
            this.textBoxMessage = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.EncryptionButton = new System.Windows.Forms.Button();
            this.encryptedDataText = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.DecoderButton = new System.Windows.Forms.Button();
            this.newMessage = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxNumberP
            // 
            this.textBoxNumberP.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.textBoxNumberP.Location = new System.Drawing.Point(44, 43);
            this.textBoxNumberP.Name = "textBoxNumberP";
            this.textBoxNumberP.Size = new System.Drawing.Size(53, 22);
            this.textBoxNumberP.TabIndex = 0;
            // 
            // textBoxNumberQ
            // 
            this.textBoxNumberQ.Location = new System.Drawing.Point(44, 71);
            this.textBoxNumberQ.Name = "textBoxNumberQ";
            this.textBoxNumberQ.Size = new System.Drawing.Size(53, 22);
            this.textBoxNumberQ.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Help;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(263, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "Введите два простых числа";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 24);
            this.label2.TabIndex = 3;
            this.label2.Text = "р:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 24);
            this.label3.TabIndex = 4;
            this.label3.Text = "q:";
            // 
            // Generate
            // 
            this.Generate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Generate.Location = new System.Drawing.Point(15, 110);
            this.Generate.Name = "Generate";
            this.Generate.Size = new System.Drawing.Size(164, 57);
            this.Generate.TabIndex = 5;
            this.Generate.Text = "Сгенерировать ключи";
            this.Generate.UseVisualStyleBackColor = true;
            this.Generate.Click += new System.EventHandler(this.Generate_Click);
            // 
            // label4
            // 
            this.infoLabel.AutoSize = true;
            this.infoLabel.Location = new System.Drawing.Point(13, 181);
            this.infoLabel.Name = "label4";
            this.infoLabel.Size = new System.Drawing.Size(0, 17);
            this.infoLabel.TabIndex = 6;
            // 
            // textBoxMessage
            // 
            this.textBoxMessage.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textBoxMessage.Location = new System.Drawing.Point(242, 71);
            this.textBoxMessage.Multiline = true;
            this.textBoxMessage.Name = "textBoxMessage";
            this.textBoxMessage.ReadOnly = true;
            this.textBoxMessage.Size = new System.Drawing.Size(278, 96);
            this.textBoxMessage.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(238, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 24);
            this.label5.TabIndex = 8;
            this.label5.Text = "Сообщение:";
            // 
            // EncryptionButton
            // 
            this.EncryptionButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EncryptionButton.Location = new System.Drawing.Point(242, 178);
            this.EncryptionButton.Name = "EncryptionButton";
            this.EncryptionButton.Size = new System.Drawing.Size(278, 40);
            this.EncryptionButton.TabIndex = 10;
            this.EncryptionButton.Text = "Зашифровать сообщение";
            this.EncryptionButton.UseVisualStyleBackColor = true;
            this.EncryptionButton.Click += new System.EventHandler(this.EncryptionButton_Click);
            // 
            // encryptedDataText
            // 
            this.encryptedDataText.Location = new System.Drawing.Point(16, 283);
            this.encryptedDataText.Multiline = true;
            this.encryptedDataText.Name = "encryptedDataText";
            this.encryptedDataText.ReadOnly = true;
            this.encryptedDataText.Size = new System.Drawing.Size(504, 59);
            this.encryptedDataText.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(12, 256);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(239, 24);
            this.label6.TabIndex = 12;
            this.label6.Text = "Зашифрованные данные:";
            // 
            // DecoderButton
            // 
            this.DecoderButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DecoderButton.Location = new System.Drawing.Point(18, 68);
            this.DecoderButton.Name = "DecoderButton";
            this.DecoderButton.Size = new System.Drawing.Size(164, 40);
            this.DecoderButton.TabIndex = 13;
            this.DecoderButton.Text = "Расшифровать ";
            this.DecoderButton.UseVisualStyleBackColor = true;
            this.DecoderButton.Click += new System.EventHandler(this.DecoderButton_Click);
            // 
            // newMessage
            // 
            this.newMessage.Location = new System.Drawing.Point(211, 21);
            this.newMessage.Multiline = true;
            this.newMessage.Name = "newMessage";
            this.newMessage.ReadOnly = true;
            this.newMessage.Size = new System.Drawing.Size(278, 121);
            this.newMessage.TabIndex = 14;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.DecoderButton);
            this.groupBox1.Controls.Add(this.newMessage);
            this.groupBox1.Location = new System.Drawing.Point(16, 348);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(504, 158);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            // 
            // Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(532, 518);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.encryptedDataText);
            this.Controls.Add(this.EncryptionButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxMessage);
            this.Controls.Add(this.infoLabel);
            this.Controls.Add(this.Generate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxNumberQ);
            this.Controls.Add(this.textBoxNumberP);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Window";
            this.Text = "Information security";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxNumberP;
        private System.Windows.Forms.TextBox textBoxNumberQ;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Generate;
        private System.Windows.Forms.Label infoLabel;
        private System.Windows.Forms.TextBox textBoxMessage;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button EncryptionButton;
        private System.Windows.Forms.TextBox encryptedDataText;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button DecoderButton;
        private System.Windows.Forms.TextBox newMessage;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}

