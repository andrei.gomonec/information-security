﻿using System;
using System.Collections.Generic;
using System.Numerics;

namespace Information_security_app
{
    class MessageEncryption
    {
        private static int _openExponent_e;
        private static int _module_n;
        private const string alphabet = " абвгдеёжзийклмнопрстуфхцчшщьыъэюя0123456789.,:!?";

        public static void GetPublicKey(int e, int n)
        {
            _openExponent_e = e;
            _module_n = n;
        }

        public void PrepareMessage(string message)
        {
            List<int> _lineElement = new List<int>();

            foreach (char item in message)
            {
                _lineElement.Add(alphabet.IndexOf(item) + 1);
            }

            Encrypt(_lineElement);
        }

        private void Encrypt(List<int> dataForEncryption)
        {
            List<int> encrypt = new List<int>();

            for (int i = 0; i < dataForEncryption.Count; i++)
            {
                BigInteger y = (BigInteger.Pow(dataForEncryption[i], _openExponent_e) % _module_n);
                encrypt.Add((int)y);
            }

            foreach (long item in encrypt)
            {
                SendMessage(Convert.ToString(item) + " ");
            }
        }

        private void SendMessage(string message)
        {
            Window.window.DataOutput(message, Window.OutputMethod.encrypted);
        }
    }
}
