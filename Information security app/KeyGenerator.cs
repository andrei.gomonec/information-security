﻿using System;
using System.Collections.Generic;

namespace Information_security_app
{
    class KeyGenerator
    {
        //PublicKey 
        private int _openExponent_e;

        //Public and Private Key
        private int _module_n;

        //Private Key
        private int _number_d;

        private Random rnd = new Random();
        private int _theEulerFunction;

        private static int _countGenerateClick;

        public void CreatingAPublicKey(int p, int q, int count)
        {
            _countGenerateClick += count;
            if(_countGenerateClick == 100)
            {
                Window.window.DataOutput("Не удалось создать закрытый ключ! " +
                    "\nУвеличьте значение простых чисел " +
                    "\nи повторите попытку.", Window.OutputMethod.exception);
                _countGenerateClick = 0;
                return;
            }
            //создаю список простых чисел 
            List<int> primeNumber;
            primeNumber = new List<int>();

            int _theNumberOfDivisionsWithoutRemainder;//кол-во делений без остатка 
            _module_n = p * q; //модуль 
            _theEulerFunction = (p - 1) * (q - 1); //функция Эйлера 

            for (int i = 3; i < _theEulerFunction; i++)
            {
                _theNumberOfDivisionsWithoutRemainder = 0;

                for (int e = 1; e < i; e++)
                {
                    if (i % e == 0)
                    {
                        _theNumberOfDivisionsWithoutRemainder++;
                    }
                }

                if (_theNumberOfDivisionsWithoutRemainder < 2)
                {
                    primeNumber.Add(i);
                }
            }

            for ( ; ; )
            {
                _openExponent_e = primeNumber[rnd.Next(0, primeNumber.Count)];

                if (IsCoprime(_openExponent_e, _theEulerFunction))
                {
                    SendMessage("\nПубличный ключ: e = " + _openExponent_e + ", n= " + _module_n);
                    _ = new MessageEncryption();
                    MessageEncryption.GetPublicKey(_openExponent_e, _module_n);
                    CreatingAPrivateKey();
                    return;
                }
            }
        }

        private void CreatingAPrivateKey()
        {
            int[] variant = new int[] { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47,
            59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 
                151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199};
            for (int i = 0; i < variant.Length; i++)
            {
                //Условие: (d*e)%fEuler=1
                _number_d = variant[i];
                long reduced = _number_d * _openExponent_e;
                long remains = _number_d * _openExponent_e / _theEulerFunction;
                long subtractible = remains * _theEulerFunction;

                if (1 == reduced - subtractible && _number_d != _openExponent_e)
                {
                    SendMessage("\nПриватный ключ: d = " + _number_d + ", n = " + _module_n);

                    _ = new DecoderMessage();
                    DecoderMessage.GetPrivateKey(_number_d, _module_n);
                    return;
                }

                if(i == variant.Length - 1)
                {
                    //Если в листе не было чисел удовлетворяющих d и ключ не сгенерировался,
                    //имитирую вызов функции button1_Click, что бы не кидать исключение.
                    //при использовании малых простых чисел вылетает ошибка.
                    Window.window.Generate_Click(this, new EventArgs());
                    return;
                }
            }
        }

        static bool IsCoprime(int a, int b)
        {
            if (a == b)
            {
                return a == 1;
            }
            else
            {
                if (a > b)
                {
                    return IsCoprime(a - b, b);
                }
                else
                {
                    return IsCoprime(b - a, a);
                }
            }
        }

        private void SendMessage(string message)
        {
            Window.window.DataOutput(message, Window.OutputMethod.info);
        }
    }
}
